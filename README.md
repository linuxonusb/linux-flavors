# Linux flavors

Linux has many different "distros" or distributions.

In the early days (early 90's), slackware is essentially the only viable distro. 
In the mid- to late-90's, GNU Linux, Debian, and Suse linux, etc., were among the most widely used.
Then came Redhat, Mandrake, and Fedora.


These days, for the past ten years or so,
Ubuntu has been one of the most popular distros. Ubuntu is based on Debian.

Ubuntu has many different flavors and derivatives.
Ubuntu's various "flavors" are primarily based on their differences in their primary desktop windows manager.

There is Kubuntu. There is Xubuntu. And, Lubuntu, Ubuntu MATE, etc.


Another popular Linux distro, Linux Mint, is based on Ubuntu, and it is really a derivative of Ubuntu.
Linux Mint also has a number of variants, based on their default Windows manager.


